package CajaNegra_Pr01;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class cajaNegraTest {

	@Test
	void test() {

		cajaNegra cj = new cajaNegra();
		
		cj.ohm(null, null, null);
		cj.ohm(5.0, null, null);
		cj.ohm(5.0, 4.0, null);
		cj.ohm(5.0, null, 4.0);
		cj.ohm(null, 5.0, 4.0);
		cj.ohm(5.125, 4.0, null);
		cj.ohm(5.0, null, 4.125);
		cj.ohm(null, 5.0, 4.125);
		cj.ohm(5.0, 0.0, null);
		cj.ohm(5.0, null, 0.0);
		cj.ohm(null, 5.0, 0.0);
		cj.ohm(5.0, 4.0, 3.0);
	}

}
