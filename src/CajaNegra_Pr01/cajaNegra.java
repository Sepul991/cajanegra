package CajaNegra_Pr01;

import java.text.DecimalFormat;
import java.util.Scanner;

public class cajaNegra {

	@SuppressWarnings("unused")
	public static void main(String[] args) {

		Scanner sc = new Scanner(System.in);

		/*
		 * Reto #41
		 * LA LEY DE OHM
		 * Fecha publicación enunciado: 10/10/22
		 * Fecha publicación resolución: 17/10/22
		 * Dificultad: FÁCIL
		 *
		 * Enunciado: Crea una función que calcule el valor del parámetro perdido correspondiente a la ley de Ohm.
		 * - Enviaremos a la función 2 de los 3 parámetros (V, R, I), y retornará el valor del tercero (redondeado a 2 decimales).
		 * - Si los parámetros son incorrectos o insuficientes, la función retornará la cadena de texto "Invalid values".
		 */
		        System.out.println(ohm(null, null, null));
		        System.out.println(ohm(5.0, null, null));
		        System.out.println(ohm(5.0, 4.0, null));//--
		        System.out.println(ohm(5.0, null, 4.0));//--
		        System.out.println(ohm(null, 5.0, 4.0));//--
		        System.out.println(ohm(5.125, 4.0, null));//--
		        System.out.println(ohm(5.0, null, 4.125));//--
		        System.out.println(ohm(null, 5.0, 4.125));//--
		        System.out.println(ohm(5.0, 0.0, null));//--
		        System.out.println(ohm(5.0, null, 0.0));//--
		        System.out.println(ohm(null, 5.0, 0.0));//--
		        System.out.println(ohm(5.0, 4.0, 3.0));
		
		Double v = null;
		Double r = null;
		Double i = null;
		Double temp=0d;

		System.out.println("Envia a la función 2 de los 3 parámetros (V, R, I), y retornará el valor del tercero (redondeado a 2 decimales)");
		System.out.println("Primer valor: ");
		temp=sc.nextDouble();
		if (temp!=0) {
			v=temp;			
		}
		System.out.println("Segundo valor: ");
		temp = sc.nextDouble();
		if (temp!=0) {
			r=temp;			
		}
		System.out.println("Segundo valor: ");
		temp=sc.nextDouble();
		if (temp!=0) {
			i=temp;			
		}
		System.out.println(ohm(v, r, i));

	}

	public static String ohm(Double v, Double r, Double i) {
		DecimalFormat formatter = new DecimalFormat("#.##");

		if (v != null && r != null && i == null) {
			return "I = " + formatter.format(v / r);
		} else if (v != null && i != null && r == null) {
			return "R = " + formatter.format(v / i);
		} else if (r != null && i != null && v == null) {
			return "V = " + formatter.format(r * i);
		}

		return "Invalid values";
	}

}
