package CajaNegra_Pr01;

import java.util.Scanner;

public class cajaNegra2 {


	public static void main(String[] args) {
		// TODO Auto-generated method stub
		/*
		 * Reto #6
		 * INVIRTIENDO CADENAS
		 * Fecha publicación enunciado: 07/02/22
		 * Fecha publicación resolución: 14/02/22
		 * Dificultad: FÁCIL
		 *
		 * Enunciado: Crea un programa que invierta el orden de una cadena de texto sin usar funciones propias del lenguaje que lo hagan de forma automática.
		 * - Si le pasamos "Hola mundo" nos retornaría "odnum aloH"*/



		//PRIMERA FORMA USANDO FUNCIONES//

		/*	String nomDer=sc.nextLine();
		StringBuilder nomRever = new StringBuilder(nomDer);		

		String invertida = nomRever.reverse().toString();//no acabo de entender porque tengo que poner el toString();

		System.out.println("Cadena sin invertir: " + nomDer);
		System.out.println("Cadena invertida: " + invertida);*/

		//SEGUNDA FORMA USANDO BUCLES//

		// Concatenamos los caracteres desde el final hasta el principio
		// dejandolos al final en la cadena
		// Ejemplo: Hola. Quedaria de la siguiente manera; HolaaloH

		// String cadena=sc.nextLine();

		/*for (int i = cadena.length()-1; i >= 0; i--) {
			cadena += cadena.charAt(i);
		}

		// ahora la cadena tiene el doble de caracteres, por ello
		// le eliminamos la mitad al inicio con la funcion substring

		cadena = cadena.substring(cadena.length()/2);

		System.out.println(cadena);*/

		//TERCERA FORMA
		
		Scanner sc = new Scanner (System.in);
		
		String palabra = sc.nextLine();
		 
		girarPalabra(palabra);
	}
	public static String girarPalabra (String palabra) {
		
		
		StringBuilder sb = new StringBuilder(palabra);
		int i = 0;
		int j = palabra.length()-1;
		char aux = ' ';

		while(i < j)
		{
			aux = palabra.charAt(j);
			sb.setCharAt(j, palabra.charAt(i));
			sb.setCharAt(i, aux);

			i++;
			j--;
		}
		String e = sb.toString();

		System.out.println(e);
		return e;
		
	}
	
}