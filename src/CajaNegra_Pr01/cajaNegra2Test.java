package CajaNegra_Pr01;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class cajaNegra2Test {

	@Test
	void test() {
		cajaNegra2 cj2 = new cajaNegra2();
		assertEquals("oigreS", cj2.girarPalabra("Sergio"));
		assertEquals("abeurp ed olpmejE", cj2.girarPalabra("Ejemplo de prueba"));
		assertEquals("euqocirablA", cj2.girarPalabra("Albaricoque"));
		assertEquals("ociredeF", cj2.girarPalabra("Federico"));
		assertEquals("dadelos ed sona neiC", cj2.girarPalabra("Cien anos de soledad"));
		assertEquals("acroL aicraG ociredeF", cj2.girarPalabra("Federico Garcia Lorca"));
		
	}

}
