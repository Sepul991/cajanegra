package Juego;

import java.util.Random;
import java.util.Scanner; // faltaba el java utils scanner

public class juegoJava {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner sc = new Scanner(System.in);//null;//esta mal construida la variable. se tiene qe dar valor al scanner
		System.out.print("Escriu el tu nom i cognom: ");
		String nom = sc.next();
		System.out.print("Benvingut " + nom);

		boolean end = false;
		while (end == false) {// No entraba en el bucle por que el simbolo de asignación estaba mal "="
			System.out.println("\n--------------------");
			System.out.println("Tria una opció");
			System.out.println("1. Canviar nom");
			System.out.println("2. jugar");
			System.out.println("3. sortir");
			int opcio = sc.nextInt(); // switch no puede leer valores del tipo "double". Solo "int, string, o variables enumerada **
			//PREGUNTAR QUE SON enum variables ***

			switch (opcio) {
			case 1: 
				System.out.print("Escriu el tu nom: ");
				nom = sc.next(); //he quitado el int
				System.out.println("Hola " + nom);
				break;
			case 2:
				System.out.println("Tria a un numero entre el 1 y el 10");
				int aposta = sc.nextInt();
				Random rnd = new Random();
				int numeroGuanyador= rnd.nextInt(1, 10);
				if (aposta == numeroGuanyador) {
					System.out.println("Has guanyat!, En hora bona "+nom);//Falta el ";"
				}else {
					System.out.println("Has perdut, el nombre correcte es: "+ numeroGuanyador);//falta el ";" Y estaba mal escrito "Guanyador"
				}	
				break;
			case 3://El numero que del "case" no era correcto ponia"4"
				System.out.println("Adeu!");
				break;
				//He quitado "end = false;" y he puesto el break y en principio me da todo correcto
			default:
				System.out.println("L'opció triada es incorrecte!");
			}
		}
	}

}


