package m05_juego;

import java.util.Random;
import java.util.Scanner; // faltaba el java utils scanner

public class Juego {//el nombre no coincidia con el nombre de la clase

	public static void main(String[] args) {

		Scanner sc = null;
		System.out.println("Escriu el tu nom i cognom");
		String nom = sc.next();
		System.out.println("Benvingut " + nom);

		boolean end = false;
		while (end = false) {
			System.out.println("--------------------");
			System.out.println("Tria una opció");
			System.out.println("1. Canviar nom");
			System.out.println("2. jugar");
			System.out.println("3. sortir");
			int opcio = sc.nextInt(); // switch no puede leer valores del tipo "double". Solo "int, string, o variables enumerada **
										//PREGUNTAR QUE SON enum variables ***

			switch (opcio) {
			case 1: 
				System.out.println("Escriu el tu nom");
				nom = sc.nextLine();
				System.out.println("Hola " + nom);
				break;
			case 2:
				System.out.println("Tria a un numero entre el 1 y el 10");
				int aposta = sc.nextInt();
				Random rnd = new Random();
				int numeroGuanyador= rnd.nextInt(1, 10);
				if (aposta == numeroGuanyador) {
					System.out.println("Has guanyat!, En hora bona "+nom);//Falta el ";"
				}else {
					System.out.println("Has perdut, el nombre correcte es: "+ numeroGuanyador);//falta el ";" Y estaba mal escrito "Guanyador"
				}	
				break;
			case 4:
				System.out.println("Adeu!");
				end = false;
			default:
				System.out.println("L'opció triada es incorrecte!");
			}
		}
	}
}
