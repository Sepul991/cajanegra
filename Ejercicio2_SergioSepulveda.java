package EXAMEN_M03_UF1_NF1_SS;

import java.util.Scanner;

public class Ejercicio2_SergioSepulveda {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		Scanner teclado = new Scanner(System.in);

		System.out.print("Introduce la tabla del numero que quiereas : ");
		int numtaules = teclado.nextInt();
		int num;

		int i=1;
		int j=1;
		for (i = 1 ; i <= numtaules; i++) {	
			System.out.println("Taula del " + i);
			for (j = 1; j <=10; j++) {
				num=i*j;
				System.out.println(i + "*" + j + "=" + num);
			}
		}		
	}
}
